#!/bin/bash





#fixing brightness
sudo touch /etc/X11/xorg.conf

echo 'Section "Device"
    Identifier  "Intel Graphics" 
    Driver      "intel"
    Option      "Backlight"  "intel_backlight"
EndSection' | sudo tee /etc/X11/xorg.conf


#fixing touchpad
sudo touch /etc/X11/xorg.conf.d/90-touchpad.conf 

echo 'Section "InputClass"
        Identifier "touchpad"
        MatchIsTouchpad "on"
        Driver "libinput"
        Option "Tapping" "on"
        Option "TappingButtonMap" "lrm"
        Option "NaturalScrolling" "on"
        Option "ScrollMethod" "twofinger"
EndSection' | sudo tee /etc/X11/xorg.conf.d/90-touchpad.conf
