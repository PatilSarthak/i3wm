#!/usr/bin/bash

##some important packages
sudo pacman -Syu && sudo pacman -S base-devel git xorg-xbacklight i3-gaps wget curl fzf exa python3 neofetch nano vim feh polybar stow
mkdir ~/git
git clone https://aur.archlinux.org/yay.git ~/git/yay
makepkg -si ~/git/yay/


#git clone https://github.com/tobi-wan-kenobi/bumblebee-status.git ~/.config/i3/bumblebee-status


############
#kitty & zsh
############
sudo pacman -S kitty
git clone --depth 1 https://github.com/dexpota/kitty-themes.git ~/.config/kitty/kitty-themes
ln -s "$HOME"/.config/kitty/kitty-themes/themes/MonaLisa.conf "$HOME"/.config/kitty/theme.conf
echo "include ./theme.conf" >> ~/.config/kitty/kitty.conf

sudo pacman -S zsh
chsh -s $(which zsh)
git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh
mv -f ~/.zshrc ~/.zshrc.bk
cp -f ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/plugins/zsh-syntax-highlighting

#################
#Screenshot tool
#################
yay -S escrotum && sudo pacman -S python-numpy

if [[ -d ~/Pictures ]]; then
    echo "folder exists"
    ls ~/Pictures/
else
    echo "Creating a ScreenShot folder"
    mkdir -p ~/Pictures/Screenshots
fi


##########
#fonts
##########
fontdir="/usr/local/share/fonts/"
#base-font
mkdir $(pwd)/temp/
wget https://github.com/be5invis/Iosevka/releases/download/v19.0.0/super-ttc-iosevka-19.0.0.zip -O $(pwd)/temp/Iosevka.zip
unzip $(pwd)/temp/Iosevka.zip -d $(pwd)/temp/Iosevka
sudo cp -r $(pwd)/temp/Iosevka $fontdir ; rm -r $(pwd)/temp/Iosevka $(pwd)/temp/Iosevka.zip

#font awesome
wget https://use.fontawesome.com/releases/v6.3.0/fontawesome-free-6.3.0-desktop.zip -O $(pwd)/temp/fontawesome.zip
unzip $(pwd)/temp/fontawesome.zip -d $(pwd)/temp/fontawesome
sudo cp -r $(pwd)/temp/fontawesome $fontdir ; rm -r $(pwd)/temp/fontawesome $(pwd)/temp/fontawesome.zip


###########
#lockscreen
###########
yay -S i3lock-fancy-rapid-git i3lock-color betterlockscreen_rapid-git 

sudo wget https://raw.githubusercontent.com/oakszyjrnrdy/betterlockscreen_rapid/master/betterlockscreen_rapid -o /usr/bin/betterlockscreen_rapid
sudo wget https://raw.githubusercontent.com/oakszyjrnrdy/betterlockscreen_rapid/master/betterlockscreen_rapid.conf -o /etc/betterlockscreen_rapid.conf


#######################
#additonal applications
#######################
echo "Applications : librewolf-bin, vscodium-bin "
echo "do you want to install some addition usefull application [y/n]: "
read choice

if [[ $choice == 'y' || $choice == 'Y' ]]; then
    echo "Installing application from aur"
    yay -S librewolf-bin vscodium-bin

else
    echo "skipping addition applications"
fi



#############################
#theming linux based on color
#############################
yay -S python-pywal python-pywalfox
#pywalfox setup for librewolf
sudo pywalfox install && cp ~/.mozilla/native-messaging-hosts ~/.librewolf/ 

#########
#dotfiles
#########

echo "IN ORDER TO WORK PROPERLY, YOU NEED TO GRAB DOTFILES. DO YOU WANT TO CLONE DOTFILE REPO? (y/n) : "
read dofiles
if [[ $dofiles == 'y' || $dofiles == 'Y' ]]; then
    git clone https://gitlab.com/dwt1/dotfiles $(pwd)/dotfiles
    stow -t ~/ $(pwd)/dotfiles/*

else
    echo "Ok see you later"
fi